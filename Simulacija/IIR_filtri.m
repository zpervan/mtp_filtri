close all;
clc;
%Sinusni signal
f = 750;
fs = 8192;
tsin = 0:0.1:100;
%Nezasumljeni sinusni signal
ysin = 2.5+2.5*sin(2*pi*f/fs*tsin);
%zasumljeni sinusni signal
ysin_sum = ysin + 0.35* randn(1, length(ysin));
%Racunanje koeficijenata
[b,a] = butter(4,0.78,'high');

%frekvencijski odziv
figure();
freqz(b,a);

%Racunanje koeficijenata
outhi = filter(b,a,ysin_sum);

%prikaz podataka
figure();
%subplot(1,2,1);
plot(tsin,ysin_sum);
%legend('Ulazni signal');
hold on;
%alpha(.5);
%subplot(1,2,2);
plot(tsin, outhi,'r');
%title('Filtriranje sinusnog signala IIR');
%legend('Filtrirani signal');
legend('Ulazni signal','Filtrirani signal');