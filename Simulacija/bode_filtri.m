clear;
close all;
clc;
%Vrijednosti za nisko-propusni filter
Cn=0.33e-6;
Rn=330;
%Prijenosna funkcija za NP filter
G = tf(1/(Cn*Rn),[1 1/(Cn*Rn)])
figure();
bode(G)
grid on;
title('Bodeov dijagram za NP filter')
sprintf('Frekvencija presjecanja:')
fc=1/(2*pi*Rn*Cn)

%Vrijednosti za visoko-propusni filter
Cv=0.033e-6;
Rv=330;
%Prijenosna funkcija za VP filter
G = tf([1 1],[1 1/(Cv*Rv)])
figure();
bode(G,{1,100000})
grid on;
title('Bodeov dijagram za VP filter')

%Vrijednosti za pojasno-propusni filter
Cp=0.33e-6;
Rp=430;
Lp=0.05;
%Prijenosna funkcija za PP filter
G = tf([(Rp/Lp) 1],[1 (Rp/Lp) 1/(Lp*Cp)])
figure();
bode(G)
grid on;
title('Bodeov dijagram za PP filter')
