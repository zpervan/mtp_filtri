close all;
clc;
%Nisko-propusni FIR filtar
% load chirp;
% figure();
% plot(y);
% title('Chirp signal');
%Sinusni signal
f = 750;
fs = 8192;
tsin = 0:0.1:50;
%Nezasumljeni sinusni signal
ysin = 2.5+2.5*sin(2*pi*f/fs*tsin);
%zasumljeni sinusni signal
ysin_sum = ysin + 0.75* randn(1, length(ysin));
%Vrijeme 
t = (0:length(ysin_sum)-1)/fs;
%Izracun koeficijenata za N=4, presjecna frekvencija fc = 0.48
blo = fir1(4,0.15,'low');
figure();
freqz(blo,1);
title('Frekvencijski odziv')
%Filtriranje signala sa dobivenim koeficijentima, 
outhi = filter(blo,1,ysin_sum);

figure();
%subplot(1,2,1);
alpha(0.3);
plot(tsin,ysin_sum);
hold on;
%title('Ulazni sinusni signal');
%legend('Ulazni signal')
%hold on;
%figure();
%subplot(1,2,2);
plot(tsin, outhi,'r');
%legend('Filtrirani signal')
%title('Filtrirani sinusni signal');
legend('Ulazni signal','Filtrirani signal');