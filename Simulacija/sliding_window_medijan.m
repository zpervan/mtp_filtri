function [izlVektor] = sliding_window_medijan(ulVektor)
    
    for i = 1:length(ulVektor)
        if(i==1)%Prva vrijednost
            %Učitavanje vrijednosti u vektor
            vrijednost = [ulVektor(i) ulVektor(i+1) ulVektor(i+2) ulVektor(i+3) ulVektor(i+4)];

           %Sortiranje niza
            for k = 1:length(vrijednost)
                for j = 1:length(vrijednost)
                    if(vrijednost(k)<vrijednost(j))
                        temp = vrijednost(i);
                        vrijednost(k) = vrijednost(j);
                        vrijednost(j) = temp;
                    end
                end
            end
            izlaz(i) = vrijednost(3);
        
        elseif(i==2) %Druga vrijednost
            %Učitavanje vrijednosti u vektor
            vrijednost = [ulVektor(i-1) ulVektor(i) ulVektor(i+1) ulVektor(i+2) ulVektor(i+3)];
            
            %Sortiranje niza
            for k = 1:length(vrijednost)
                for j = 1:length(vrijednost)
                    if(vrijednost(k)<vrijednost(j))
                        temp = vrijednost(k);
                        vrijednost(k) = vrijednost(j);
                        vrijednost(j) = temp;
                    end
                end
            end
            izlaz(i) = vrijednost(3);
            
        elseif(i==(length(ulVektor)-1))
            %Učitavanje vrijednosti u vektor
            vrijednost = [ulVektor(i-3) ulVektor(i-2) ulVektor(i-1) ulVektor(i) ulVektor(i+1)];
            
            %Sortiranje niza
            for k = 1:length(vrijednost)
                for j = 1:length(vrijednost)
                    if(vrijednost(k)<vrijednost(j))
                        temp = vrijednost(k);
                        vrijednost(k) = vrijednost(j);
                        vrijednost(j) = temp;
                    end
                end
            end
            izlaz(i) = vrijednost(3);
        
        elseif(i==(length(ulVektor)))
            %Učitavanje vrijednosti u vektor
            vrijednost = [ulVektor(i-4) ulVektor(i-3) ulVektor(i-2) ulVektor(i-1) ulVektor(i)];
            
            %Sortiranje niza
            for k = 1:length(vrijednost)
                for j = 1:length(vrijednost)
                    if(vrijednost(k)<vrijednost(j))
                        temp = vrijednost(k);
                        vrijednost(k) = vrijednost(j);
                        vrijednost(j) = temp;
                    end
                end
            end
            izlaz(i) = vrijednost(3);
        else
            %Učitavanje vrijednosti u vektor
            vrijednost = [ulVektor(i-2) ulVektor(i-1) ulVektor(i) ulVektor(i+1) ulVektor(i+2)];
            
            %Sortiranje niza
            for k = 1:length(vrijednost)
                for j = 1:length(vrijednost)
                    if(vrijednost(k)<vrijednost(j))
                        temp = vrijednost(k);
                        vrijednost(k) = vrijednost(j);
                        vrijednost(j) = temp;
                    end
                end
            end
            izlaz(i) = vrijednost(3);
        end
    end
    izlVektor = izlaz;
end